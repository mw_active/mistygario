#!/usr/bin/env bash

if [[ $EUID -ne 0 ]]; then
   echo "This script must be run as root"
   exit 1
fi

# Exit if any step fails
set -e

if [ "$1" == "-e" ]; then
    echo "Using external IP address"
    ip_address=$(curl https://ipinfo.io/ip)
else
    echo "Using internal IP address"
    ip_address=$(hostname -I | awk '{print $1}')
fi

port=443
target_file=/var/www/html/index.html

echo "Current IP: $ip_address"
echo "Target file: $target_file"

# Replace with current IP address, and select as default option
line_to_replace="<option value=\"${ip_address}:${port}\">MistyWest</option>"
sed -ie "/<option value=/c\\${line_to_replace}" $target_file

echo "Update complete. Now serving at $ip_address on port $port!"
