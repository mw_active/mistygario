# mistygario

MistyWest's version of Agario, based on [MultiOgar-Edited](https://github.com/Megabyte918/MultiOgar-Edited) (server) and [Cigar](https://github.com/CigarProject/Cigar/tree/a2ea309345a8fb03991eba0d63363740aae7f5a5/) (client). Hosted using Apache.

## Features

- Experimental mode with red viruses
- Custom command to start a timed game of `N` minutes, with messages displayed in chat of time remaining
- Smaller map geared for fewer players

## Getting Started

### Linux

#### Installation

To install all required components:

```
sudo ./install-linux.sh
```

### Windows (Untested)

It's also possible to install this on Windows. Follow steps [here](https://github.com/Megabyte918/MultiOgar-Edited#windows), install Apache server, then copy files from `external/Cigar/www` to wherever Apache (on Windows) expects them.

## Running Agario

To start the server:

```bash
# If run manually
cd external/MultiOgar-Edited/src
sudo node index.js

# Equivalent using the alias in ~/.bashrc
run-agario
```

Games are active by default. Type `p` or `pause` to toggle between pause/resume.

To start a timed game:

```bash
# In minutes. Can also use 't 15' instead.
timer 15
```

Type `x` or `exit` to close the server. Type `h` or `help` to show all available commands.

### Using an External Server

If hosting on an external server, the client webpage needs to be updated with the external IP (installation script uses internal IP):

```bash
# Update files with the current external IP
sudo ./update-ip-address.sh -e
```

It's also recommended to use `screen` so the game continues in the event that you disconnect from your ssh session. If necessary, detach using `ctrl+a,d`.

```bash
# Create a new session
screen

# Reattach from a detached session
screen -r
```

See [AWS.md](AWS.md) for details on running it automatically on AWS.

## Changing Configs

### IP Address

The following script is provided to update the server's IP address whenever the IP address changes. Simply restart the agario server for the change to take into effect (no need to reload Apache).

```bash
# If using internal IP
sudo ./update-ip-address.sh
```

### Game Settings

Game settings can be modified by editing `external/MultiOgar-Edited/gameserver.ini`.

### Apache Server

To start/stop the apache server (it's always on by default, but it's good to stop any possible incoming traffic when not in use):

```bash
sudo service apache2 stop
sudo service apache2 start

# To check the status
sudo service apache2 status
```

## Tested On:

### Software

| OS | npm version | node version |
|---|---|---|
| Ubuntu 18.04 LTS | 6.13.7 | 13.11.0 |
| Ubuntu 16.04.6 | 6.4.1 | 10.15.0 |
| Rasbian 10 (buster) | 5.8.0 | 10.15.2 |

### Servers

| Host | Specs | Comments |
|---|---|---|
| Raspberry Pi 3 B+ | 64-bit SoC @ 1.4GHz, 1GB SDRAM | Reliable, inexpensive choice if running locally |
| Google Cloud Platform | n1-standard-2 (2 vCPU, 7.5 GB memory) | Laggy, even when static IP address was upgraded from Standard to Premium networking. Upgrading to n1-standard-4 or n1-standard-8 didn't make a difference. |
| AWS | t3a.medium (2 vCPU, 4 GB memory, 5 Gbps burst bandwidth) | Runs better than GCP |

## Contributors

- Walker
- Justin
