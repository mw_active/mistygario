# Run Agario Automatically on AWS

The overview of steps is as follows:

1. On server, setup agario in a crontab (in a detached screen session so it can be resumed when you ssh in)
2. On AWS, create Lambda functions to start/stop the EC2 instance, triggered by CloudWatch events

Workflow:

1. At 3:45pm PST, CloudWatch triggers Lambda function to start EC2 instance
2. At 3:55pm PST, crontab triggers and agario server is started (in a paused state)
3. Between 3:55-4:00pm PST, ssh into the server and type `screen -r` to enter the agario control screen
4. At 4:01pm PST, agario will start a timed game for 15 mins
5. At 4:16pm PST, agario game ends and final player leaderboard is shown. Agario shuts down 1 min after. Ssh out of server at this step.
6. At 4:30pm PST, CloudWatch triggers another Lambda function to stop EC2 instance

## Setup Crontab
To set up agario so it automatically runs, add the following line to your crontab:

```bash
# Time is in UTC, so -7 for PST
# Start the server at 3:55PM PST in a detached screen session, start the game 6 mins after for 15 mins. Save output to timestamped log
55 22 * * * cd ~/mistygario/external/MultiOgar-Edited/src;screen -L -Logfile ~/mistygario/logs/$(date +\%Y-\%m-\%d_\%H-\%M-\%S).log -dm sudo node index.js --timer 15 --delay 6
```

## Start and Stop EC2 Instances with Lambda

Source: https://aws.amazon.com/premiumsupport/knowledge-center/start-stop-lambda-cloudwatch/

### Create an IAM policy and role

1.    Create an IAM policy using the JSON policy editor. Paste this JSON policy document into the policy editor:

```json
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Action": [
        "logs:CreateLogGroup",
        "logs:CreateLogStream",
        "logs:PutLogEvents"
      ],
      "Resource": "arn:aws:logs:*:*:*"
    },
    {
      "Effect": "Allow",
      "Action": [
        "ec2:Start*",
        "ec2:Stop*"
      ],
      "Resource": "*"
    }
  ]
}
```

2.    Create an IAM role for Lambda. When attaching a permissions policy, search for and choose the IAM policy that you created.

### Create Lambda functions that stop and start your EC2 instances

1.    In the Lambda console, choose Create function.

2.    Choose Author from scratch.

3.    Under Basic information, add the following:
For Function name, enter a name that identifies it as the function used to stop your EC2 instances. For example, "StopEC2Instances".
For Runtime, choose Python 3.7.
Under Permissions, expand Choose or create an execution role.
Under Execution role, choose Use an existing role.
Under Existing role, choose the IAM role that you created.

4.    Choose Create function.

5.    Copy this code, and then under Function code, paste it into the editor pane in the code editor (lambda_function). This code stops the EC2 instances that you identify.

Note: For region, replace "us-west-1" with the AWS Region that your instances are in. For instances, replace the example EC2 instance IDs with the IDs of the specific instances that you want to stop and start.

```python
import boto3
region = 'us-west-1'
instances = ['i-12345cb6de4f78g9h', 'i-08ce9b2d7eccf6d26']
ec2 = boto3.client('ec2', region_name=region)

def lambda_handler(event, context):
    ec2.stop_instances(InstanceIds=instances)
    print('stopped your instances: ' + str(instances))
```

6.    Under Basic settings, set Timeout to 10 seconds.

Note: Configure the Lambda function settings as needed for your use case. For example, if you want to stop and start multiple instances, you might need a different value for Timeout, as well as Memory.

7.    Choose Save.

8.    Repeat steps 1-7 to create another function. Do the following differently so that this function starts your EC2 instances:
In step 3, enter a Function name it as the function used to start your EC2 instances. For example, "StartEC2Instances".
In step 5, copy and paste this code into the editor pane in the code editor (lambda_function):

Note: For region and instances, use the same values that you used for the code to stop your EC2 instances.

```python
import boto3
region = 'us-west-1'
instances = ['i-12345cb6de4f78g9h', 'i-08ce9b2d7eccf6d26']
ec2 = boto3.client('ec2', region_name=region)

def lambda_handler(event, context):
    ec2.start_instances(InstanceIds=instances)
    print('started your instances: ' + str(instances))
```

### Test your Lambda functions

1.    In the Lambda console, choose Functions.

2.    Select one of the functions that you created.

3.    Choose Actions, and then choose Test.

4.    In the Configure test event dialog, choose Create new test event.

5.    Enter an Event name, and then choose Create.

Note: You don't need to change the JSON code for the test event—the function doesn't use it.

6.    Choose Test to execute the function.

7.    Repeat steps 1-6 for the other function that you created.

Tip: You can check the status of your EC2 instances before and after testing to confirm that your functions work as expected.

### Create rules that trigger your Lambda functions

1.    Open the CloudWatch console.

2.    In the left navigation pane, under Events, choose Rules.

3.    Choose Create rule.

4.    Under Event Source, choose Schedule.

5.    Enter the following cron expression (every weekday at 4:30pm PST): `30 23 ? * MON-FRI *`

6.    Under Targets, choose Add target.

7.    Choose Lambda function.

8.    For Function, choose the function that stops your EC2 instances.

9.    Choose Configure details.

10.    Under Rule definition, do the following:
For Name, enter a name to identify the rule, such as "StopEC2Instances".
(Optional) For Description, describe your rule. For example, "Stops EC2 instances every night at 10 PM."
For State, select the Enabled check box.

11.    Choose Create rule.

12.    Repeat steps 1-11 to create a rule to start your EC2 instances. Do the following differently:
In step 5, for Cron expression, enter (every weekday at 3:45pm PST): `45 22 ? * MON-FRI *`
In step 8, for Function, choose the function that starts your EC2 instances.
In step 10, under Rule definition, enter a Name like "StartEC2Instances", and optionally enter a Description like "Starts EC2 instances every morning at 6 AM."
