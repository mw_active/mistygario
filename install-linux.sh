#!/usr/bin/env bash

if [[ $EUID -ne 0 ]]; then
   echo "This script must be run as root"
   exit 1
fi

# Exit if any step fails
set -e

echo "Downloading requirements..."

curl -sL https://deb.nodesource.com/setup_14.x | sudo -E bash -
sudo apt-get install -y nodejs

apt install -y apache2

cigar_source=external/Cigar/www/
cigar_target=/var/www/html/

echo -e "\nCopying files from $cigar_source to $cigar_target..."
# Copy webpage files for server
mkdir -p $cigar_target
cp -r $cigar_source* $cigar_target

echo -e "\nInstalling Agario..."
# Install agario
cd external/MultiOgar-Edited
npm install

cd ../../
echo -e "\nUpdating IP address..."
./update-ip-address.sh

echo -e "\nEditing server webpage from default..."
cigar_index_file=${cigar_target}index.html
sed -i "s/Home - Cigar/Home - MistyGario/" $cigar_index_file
sed -i 's/>CigarProject</>MistyWest Agario</' $cigar_index_file
sed -i 's/placeholder="Nick"/placeholder="Josh"/' $cigar_index_file

echo "alias run-agario='cd ~/mistygario/external/MultiOgar-Edited/src;sudo node index.js'" >> ~/.bashrc
source ~/.bashrc

echo -e "\nInstallation complete. Execute 'run-agario' to start the server! Or run `./update-ip-address.sh -e` if using an external IP address."
